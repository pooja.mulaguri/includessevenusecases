from scripts.core.usecases import Usecases
input1=int(input("enter the usecase from 1 to 7"))
if(input1==1):
    data=["hari","balu","paul","john"]
    Usecases.usecase1(data)
elif(input1==2):
    data=["Andhra Pradesh","Arunachal Pradesh ","Assam","Bihar","Chhattisgarh","Goa","Gujarat","Haryana",
          "Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Madhya Pradesh",
          "Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha","Punjab","Rajasthan",
          "Sikkim","Tamil Nadu","Telangana","Tripura","Uttar Pradesh","Uttarakhand","West Bengal",
          "Andaman and Nicobar Islands","Chandigarh","Dadra and Nagar Haveli","Daman and Diu",
          "Lakshadweep","National Capital Territory of Delhi","Puducherry"]
    Usecases.usecase2(data)
elif(input1==3):
    data = ["Andhra Pradesh", "Arunachal Pradesh ", "Assam", "Bihar", "Chhattisgarh", "Goa", "Gujarat", "Haryana",
            "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Madhya Pradesh",
            "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan",
            "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal",
            "Andaman and Nicobar Islands", "Chandigarh", "Dadra and Nagar Haveli", "Daman and Diu",
            "Lakshadweep", "National Capital Territory of Delhi", "Puducherry"]
    Usecases.usecase3(data)
elif(input1==4):
    my_file = open("file.txt", "r")
    content = my_file.read()
    Usecases.usecase4(content)
elif(input1==5):
    data=[{"Name": "Andhra Pradesh", "population": "49,386,799", "Rank": "7"},
            {"Name": "Uttar Pradesh", "population": " 199,812,341", "Rank": "4"},
            {"Name": "Maharashta", "population": "112,372,972", "Rank": "3"},
            {"Name": "Bihar", "population": " 103,804,637", "Rank": "12"},
            {"Name": "West Bengal", "population": "91,347,736", "Rank": "13"},
            {"Name": "Madhya Pradesh", "population": "72,597,565", "Rank": "2"},
            {"Name": "Tamil Nadu", "population": "72,138,958", "Rank": "10"},
            {"Name": "Rajasthan", "population": "68,621,012", "Rank": "1"},
            {"Name": "Karnataka", "population": "61,130,704", "Rank": "6"},
            {"Name": "Gujarat", "population": "60,383,628", "Rank": "5"},
            {"Name": "Odisha", "population": "41,947,358", "Rank": "8"},
            {"Name": "Telangana", "population": "35,286,757", "Rank": "11"},
            {"Name": "Kerala", "population": "3,387,677", "Rank": "21"},
            {"Name": "Jharkhand", "population": " 32,966,238", "Rank": "15"},
            {"Name": "Assam", "population": "31,169,272", "Rank": "16"},
            {"Name": "Punjab", "population": "27,704,236", "Rank": "19"},
            {"Name": "Chattisgarh", "population": "25,540,196", "Rank": "9"},
            {"Name": "Haryana", "population": "25,353,081", "Rank": "20"},
            {"Name": "Jammu and Kashmir", "population": "12,548,926", "Rank": "UT6"},
            {"Name": "Uttarakhand", "population": "10,116,752", "Rank": "18"},
            {"Name": "Himachal Pradesh", "population": "6,864,602", "Rank": "17"},
            {"Name": "Tripura", "population": "3,671,032", "Rank": "26"},
            {"Name": "Meghalaya", "population": "2,964,007", "Rank": "22"},
            {"Name": "Manipur", "population": "2,721,756", "Rank": "23"},
            {"Name": "Nagaland", "population": "1,980,602", "Rank": "25"},
            {"Name": "Goa", "population": "1,457,723", "Rank": "28"},
            {"Name": "Arunachal Pradesh", "population": "1,382,611", "Rank": "14"},
            {"Name": "Mizoram", "population": "1,091,014", "Rank": "24"},
            {"Name": "Sikkim", "population": "607,688", "Rank": "27"},
            ]
    Usecases.usecase5(data)
elif(input1==6):
    Usecases.usecase6()
elif(input1==7):
    Usecases.usecase7()
else:
    print("requested data is outof range pls select within 1 to 6 numbers")

